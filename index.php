<?php
    include_once 'server.php';
?>

<!DOCTYPE html>
<html>
<head>
    <title>WIC Task</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="col-md-6">
            <div id="error_div"></div>
            <div class="form-group">
                <label>Country</label>
                <select class="form-control" name="country" id="country">
                    <option></option>
                    <?php if (isset($_SESSION['countries'])) {
                        $countries = $_SESSION['countries'];
                        foreach ($countries as $country) {
                    ?>
                    <option class="countryOption" data-id="<?= $country['id']; ?>" data-code="<?= $country['code']; ?>"><?= $country['country']; ?>/<?= $country['code']; ?></option>

                    <?php
                        }
                    } ?>
                </select>
            </div>

            <div class="form-group">
                <label>Zip Code</label>
                <input type="text" name="zip_code" id="zip_code" class="form-control">
            </div>

            <div class="form-group">
                <button type="button" name="action" value="search" id="button" class="btn btn-success">search</button>
            </div>
        </div>

        <div id="result_div" class="col-md-6"></div>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
</html>

<?php session_destroy(); ?>