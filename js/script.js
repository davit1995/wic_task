$(document).ready(function(){

    $('#button').on('click',function(){
        var country = $('#country').val(),
            zip_code = $('#zip_code').val();

        if (country.length == 0 || zip_code.length == 0) {
            $('#error_div').addClass('alert alert-danger');
            $('#error_div').html('Please fill all the felds!');
        } else {
            $('#error_div').removeClass('alert alert-danger');
            $('#error_div').empty();

            $.ajax({
                url: 'server.php',
                type: 'post',
                data: {country: country, zip_code: zip_code, action: 'search'},
                success: function(r){
                    $('#result_div').empty();
                    if (r) {
                        r = JSON.parse(r);
                        if (r.status == 'error') {
                            var url = r.url;
                            zippoReq(url);
                        } else {
                            var text = '';
                            for (var i = 0; i < r.data.length ; i++) {
                                var place_name = r.data[i].place_name,
                                    state = r.data[i].state,
                                    lat = r.data[i].latitude,
                                    long = r.data[i].longitude;

                                text += "<div>"
                                        +   "<h2>" + place_name + " / " + state + "</h2>"
                                        +   "<h3> Latitude: " + lat + ", Longitude: " + long + "</h3>"
                                    + "</div> <hr>";
                                $('#result_div').append(text);
                            }
                        }
                    }
                }
            })
        }
    })

    function zippoReq(url){
        var client = new XMLHttpRequest();
        client.open("GET", url , true);
        client.onreadystatechange = function() {
            if(client.readyState == 4) {
                checkRes(client.responseText);
            };
        };

        client.send();
    }

    function checkRes(response){

        $('#result_div').empty();

        response = JSON.parse(response);
        if (!response.places) {
            $('#error_div').addClass('alert alert-danger');
            $('#error_div').html('Incarect Zip Code!');
        } else {
            $('#error_div').removeClass('alert alert-danger');
            $('#error_div').empty();

            var post_code,
                country = response.country,
                text = '';

            $.each(response, function(key,val){
                if (key == 'post code') {
                    post_code = val;
                }
            });

            for (var i = 0; i < response.places.length ; i++) {
                var place_name,
                    state = response.places[i].state,
                    lat = response.places[i].latitude,
                    long = response.places[i].longitude;

                $.each( response.places[i], function( key, value ) {
                    if (key == 'place name') {
                        place_name = value;
                    }
                });

                text += "<div>"
                        +   "<h2>" + place_name + " / " + state + "</h2>"
                        +   "<h3> Latitude: " + lat + ", Longitude: " + long + "</h3>"
                    + "</div> <hr>";
                $('#result_div').append(text);

                $.ajax({
                    url: 'server.php',
                    type: 'post',
                    data: {country: country, post_code: post_code, state: state, lat: lat, long: long, place_name: place_name, action: 'add_items'},
                    success: function(){}
                })
            }
        }
    }
})