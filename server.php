<?php
session_start();
class GetLocation
{

    private $db;
    private $zippo = 'http://api.zippopotam.us/';

    function __construct() {
        $this->db = mysqli_connect('localhost', 'root', '', 'wic_test');

        $_SESSION['countries'] = $this->get_info("countries");

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            call_user_func(array($this, $_POST['action']));
        }
    }

    function search(){
        $country_name = explode('/', $_POST['country'])[0];
        $country_code = explode('/', $_POST['country'])[1];
        $zip_code = $_POST['zip_code'];
        $country_id = $this->get_info('countries', 'id', array('country' => $country_name) )[0]['id'];

        $ifExistZip = $this->get_info('zip_codes', '*', array('zip_code' => $zip_code, 'country_id' => $country_id) );

        $url = $this->zippo . strtolower($country_code) . '/' . $zip_code;

        $response = [];
        if (!empty($ifExistZip)) {
            $zip_code_id = $ifExistZip[0]['id'];
            $ifExistLocation = $this->get_info('locations', '*', array('zip_code_id' => $zip_code_id) );

            if (!empty($ifExistLocation)) {
                $response = [
                    'status' => 'success',
                    'url' => '',
                    'data' => $ifExistLocation
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'url' => $url,
                    'data' => []
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'url' => $url,
                'data' => []
            ];
        }

        print json_encode($response);
    }

    function add_items(){

        $country = $_POST['country'];
        $zip_code = $_POST['post_code'];
        $state = $_POST['state'];
        $place_name = $_POST['place_name'];
        $lat = $_POST['lat'];
        $long = $_POST['long'];

        $country_id = $this->get_info('countries', 'id', array('country' => $country) )[0]['id'];

        $ifExistZip = $this->get_info('zip_codes', '*', array('zip_code' => $zip_code, 'country_id' => $country_id) );

        if (!empty($ifExistZip)) {

            $zip_code_id = $ifExistZip[0]['id'];

        } else {

            $fields = array('country_id', 'zip_code');
            $values = array($country_id, $zip_code);

            $this->insert_info('zip_codes', $fields, $values);

            $zip_code_id = $this->db->insert_id;

        }

        $fields = array('zip_code_id', 'place_name', 'state', 'longitude', 'latitude');
        $values = array($zip_code_id, $place_name, $state, $long, $lat);

        $this->insert_info('locations', $fields, $values);
    }

    private function get_info($table, $what = '*', $where = array()) {
        if (empty($where)) {

            $query = "SELECT $what FROM $table";

        } else {

            $where_str = '';
            foreach ($where as $key => $value) {
                $where_str .= $key . "= '" . $value . "'";
                if ($value != end($where)) {
                    $where_str .= " and ";
                }
            }

            $query = "SELECT $what FROM $table WHERE $where_str";

        }

        $result = $this->db->query($query)->fetch_all(MYSQLI_ASSOC);
        
        return $result;
    }

    private function insert_info($table,$fields,$values) {
        $fields = implode(',', $fields);
        $values = "'" . implode("', '", $values) . "'";

        $query = "INSERT INTO $table($fields) values($values)";

        $this->db->query($query);
    }
}

$obj = new GetLocation();
?>